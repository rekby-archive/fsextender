module github.com/rekby/fsextender

go 1.14

require (
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/ogier/pflag v0.0.1
	github.com/rekby/gpt v0.0.0-20200219180433-a930afbc6edc
	github.com/rekby/mbr v0.0.0-20190325193910-2b19b9cdeebc
	github.com/rekby/pretty v0.0.0-20150927081721-c162edfa0cca
)
